package com.example.yahoovideos.videosDetail

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.yahoovideos.R
import com.example.yahoovideos.mainActivity.CustomViewHolder
import com.example.yahoovideos.mainActivity.DetailVideo
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import kotlinx.android.synthetic.main.activity_main.*
import okhttp3.*
import java.io.IOException

class VideoDetailActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(R.layout.activity_main)
        recyclerView_main.layoutManager = LinearLayoutManager(this)
        recyclerView_main.adapter = VideoDetailAdapter()

        val navBarTitle = intent.getStringExtra(CustomViewHolder.VIDEO_TITLE_KEY)
        supportActionBar?.title = navBarTitle

        fetchJSON()
    }
    private fun fetchJSON(){
        val videoId = intent.getIntExtra(CustomViewHolder.VIDEO_ID_KEY, -1)
        val videoDetailUrl = "http://api.letsbuildthatapp.com/youtube/course_detail?id="+videoId

        val client = OkHttpClient()
        val request = Request.Builder().url(videoDetailUrl).build()
        client.newCall(request).enqueue(object: Callback{
            override fun onResponse(call: Call, response: Response) {
                val body = response?.body?.string()

                val gson = GsonBuilder().create()
                val detailsVideo =  gson.fromJson(body, Array<DetailVideo>::class.java)


            }
            override fun onFailure(call: Call, e: IOException) {

            }



        })
    }
}
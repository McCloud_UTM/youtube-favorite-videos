package com.example.yahoovideos.mainActivity

import android.content.Intent
import android.view.View
import androidx.recyclerview.widget.RecyclerView
import com.example.yahoovideos.videosDetail.VideoDetailActivity

class HomeFeed(val videos: List<Video>)

class Video(val id: Int, val name: String, val link: String, val imageUrl: String,
            val numberOfViews: Int, val channel: Channel
)

class Channel(val name: String, val profileImageUrl: String)

class DetailVideo(val name:String, val duration:String, val number:Int, val imageUrl:String, val link:String)

class CustomViewHolder(val view: View, var video: Video? = null): RecyclerView.ViewHolder(view){
    companion object{
        val VIDEO_ID_KEY = "VIDEO_ID"
        val VIDEO_TITLE_KEY = "VIDEO_TITLE"
    }
    init {
        view.setOnClickListener {
            val intent = Intent(view.context, VideoDetailActivity::class.java)
            intent.putExtra(VIDEO_TITLE_KEY, video?.name)
            intent.putExtra(VIDEO_ID_KEY, video?.id)
            view.context.startActivity(intent)
        }
    }
}

package com.example.yahoovideos.videosDetail

import android.graphics.Color
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.yahoovideos.R

class VideoDetailAdapter: RecyclerView.Adapter<VideoDetailViewHolder>(){
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): VideoDetailViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val customView = layoutInflater.inflate(R.layout.video_details_row, parent, false)
        return VideoDetailViewHolder(customView)
    }

    override fun getItemCount(): Int {
        return 1
    }

    override fun onBindViewHolder(holder: VideoDetailViewHolder, position: Int) {

    }

}
class VideoDetailViewHolder(private val customView: View): RecyclerView.ViewHolder(customView){
    init{
        customView.setOnClickListener{
            println("load webview")
        }
    }
}